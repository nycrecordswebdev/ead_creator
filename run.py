# -*- coding: UTF-8 -*-
"""
run.py
"""
from flask import Flask, render_template  # Import class Flask from module flask
from forms import EADHeaderForm
app = Flask(__name__)    # Construct an instance of Flask class
app.config['SECRET_KEY'] = 'YOUR-SECRET'

@app.route('/')
def index():
   form = EADHeaderForm()
   """Render an HTML template and return"""
   return render_template('ead_input.html', form=form)

if __name__ == '__main__':  # Script executed directly?
   app.run(debug=True)  # Launch built-in web server and run this Flask webapp
